<?php

class Person{

    public $name="Test Name";
    public $phone="0119901990";
    public $dateOfBirth="01-01-1999";

    public static $message;

    public static function doFrequently(){

        echo "I'm doing it frequently... <br>";

    }

    public function doSomething(){

        echo "I'm doing something here"."<br>";

    }

    public function __construct()
    {
        echo "I'm inside the ".__METHOD__."<br>";
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        echo "I'm dying, I'm inside the ".__METHOD__."<br>";
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        echo "I'm inside the ".__METHOD__."<br>";

        echo "name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }

    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        echo "I'm inside the static ".__METHOD__."<br>";

        echo "wrong static name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
        echo "Wrong Property name= $name<br>";
        echo "Value tried to set to that wrong property = $value<br>";
    }

    public function __get($name)
    {
        // TODO: Implement __get() method.
        echo __METHOD__. " Wrong property name = $name.<br>";
    }

    public function __isset($name)
    {
        // TODO: Implement __isset() method.
        echo __METHOD__." Wrong property name = $name<br>";
    }

    public function __unset($name)
    {
        // TODO: Implement __isset() method.
        echo __METHOD__." Wrong property name = $name<br>";
    }

    public function __sleep(){
        return array("name","phone");
    }

    public function __wakeup()
    {
        // TODO: Implement __wakeup() method.
        //do any task here
        $this->doSomething();
    }



    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Are you Crazy? Don't forget I'm an Object.<br>";
    }

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
        echo "I'm an Object but you are trying to call me as a function! <br>";
    }
}   //end of person class

Person::$message= "Any message here"; //static property setting test
echo Person::$message."<br>";         //static property reading test

/*
//wrong static method call
Person::doFrequentliii("wrong static para 1", "wrong static para 2"); //callStatic()

*/
$obj= new Person();        // __construct

$obj->doSomething();       //user defined method call

$obj->habijabi("para_habi1","para_jabi1");            //call()

$obj->address= "5/2 Gol Pahar Mor, Ctg.". "<br>";    //__set()

echo $obj->address;                                  //__get()
echo "<br>";


if( isset ($obj->naiProperty) ) {          // __isset()

}

unset($obj->naiProperty);                  // __unset()



$myVar = serialize($obj);
echo "<pre>";
var_dump($myVar);
echo "</pre>";

echo "<pre>";
var_dump(unserialize($myVar));
echo "</pre>";


echo $obj; // __toString()


$obj();

?>