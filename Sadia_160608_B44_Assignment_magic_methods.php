<?php

class Person
{
    public $name = "Test Name";
    public $phone = "0119901990";
    public $dateOfBirth = "01-01-1999";

    public static $message;

    public function doSomething()
    {

        echo "I'm doing something here" . "<br>";

    }

    public function __construct()
    {
        echo "I'm inside the " . __METHOD__ . "<br>";
    }

    public function __destruct()
    {
        echo "I'm dying, I'm inside the " . __METHOD__ . "<br>";
    }

    public function __call($name, $arguments)
    {
        echo "I'm inside the " . __METHOD__ . "<br>";

        echo "name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }

    public static function __callStatic($name, $arguments)
    {
        echo "I'm inside the static " . __METHOD__ . "<br>";

        echo "wrong static name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public static function doFrequently()
    {

        echo "I'm doing it frequently... <br>";

    }

    public function __set($name, $value)
    {
        echo "Wrong Property name= $name<br>";
        echo "Value tried to set to that wrong property = $value<br>";
    }

    public function __get($name)
    {
        echo __METHOD__ . " Wrong property name = $name.<br>";
    }

    public function __isset($name)
    {
        echo __METHOD__ . " Wrong property name = $name<br>";
    }

    public function __unset($name)
    {
        echo __METHOD__ . " Wrong property name = $name<br>";
    }

    public function __sleep()
    {
        return array($this->name, $this->phone);
    }

    public function __wakeup()
    {
        //do any task here
        $this->doSomething();
    }

    public function __toString()
    {
        return "Are you Crazy? Don't forget I'm an Object.<br>";
    }


    public function __invoke()
    {
        echo "I'm an Object but you are trying to call me as a function! <br>";
    }
}

$obj= new Person();


echo $obj; // __toString()

$obj();


?>